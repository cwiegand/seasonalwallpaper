package mobi.wiegandtech.wallpaper.seasonal;

import java.util.GregorianCalendar;
import java.util.Random;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.os.Handler;
import android.service.wallpaper.WallpaperService;
import android.view.SurfaceHolder;

public class SeasonalWallpaper extends WallpaperService {
	private final Handler mHandler = new Handler();

	@Override
	public void onCreate() {
		super.onCreate();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public Engine onCreateEngine() {
		// TODO Auto-generated method stub
		return new SeasonEngine();
	}

	public class SeasonEngine extends Engine {
		private final static int COLOR_SKY_DAY = 0xff7080ff; // light blue
		private final static int COLOR_SKY_NIGHT = 0xff07071f; // dark blue
		private final static int COLOR_SUN_HALO = 0xdfffff55; // lighter yellow
		private final static int COLOR_SUN = 0xffffff55; // light yellow
		private final static int COLOR_MOON = 0xffefffff; // white
		private final static int COLOR_GROUND = 0xff200e00; // brown
		private final static int COLOR_GRASS = 0xff008f00; // green
		private final static int COLOR_BUILDING = 0xff7f7f7f; // grey
		private final static int COLOR_WINDOW_LIT = 0xffffff9f; // yellow
		private final static int COLOR_WINDOW_DARK = 0xff090909; // dark dark
		// grey
		private final static int REFRESH_TIMING = 1000 * 60 * 5; // 5 minutes
		private final static float SCALE_GROUND_HEIGHT = 0.2f; // 20%
		private final static float SCALE_BUILDING_HEIGHT = 0.5f; // 50%
		private final static float SCALE_BUILDING_LEFT = 0.1f; // 10%
		private final static float SCALE_BUILDING_RIGHT = 0.5f; // 50%
		private final static float SCALE_SKY_HEIGHT = 0.8f; // 80%
		private final static float SCALE_SUN_RADIUS = 0.1f; // 10%
		private final static float SCALE_SUN_X = 0.8f; // 80%
		private final static float SCALE_SUN_Y = 0.2f; // 20%
		private boolean mVisible = false;

		SeasonEngine() {
		}

		private final Runnable mThread = new Runnable() {
			public void run() {
				draw();
			}
		};

		private void draw() {
			final SurfaceHolder holder = getSurfaceHolder();
			Canvas c = null;
			try {
				c = holder.lockCanvas();
				if (c != null) {
					c.save();
					drawWallpaper(c);
					c.restore();
				}
			} finally {
				if (c != null)
					holder.unlockCanvasAndPost(c);
			}
			mHandler.removeCallbacks(mThread);
			if (mVisible)
				mHandler.postDelayed(mThread, REFRESH_TIMING);
		}

		@Override
		public void onCreate(SurfaceHolder surfaceHolder) {
			super.onCreate(surfaceHolder);
		}

		@Override
		public void onDestroy() {
			super.onDestroy();
			mHandler.removeCallbacks(mThread);
		}

		@Override
		public void onVisibilityChanged(boolean visible) {
			mVisible = visible;
			if (visible)
				draw();
			else
				mHandler.removeCallbacks(mThread);
		}

		@Override
		public void onSurfaceChanged(SurfaceHolder holder, int format,
				int width, int height) {
			super.onSurfaceChanged(holder, format, width, height);
			draw();
		}

		@Override
		public void onSurfaceCreated(SurfaceHolder holder) {
			super.onSurfaceCreated(holder);
		}

		@Override
		public void onSurfaceDestroyed(SurfaceHolder holder) {
			super.onSurfaceDestroyed(holder);
			mVisible = false;
			mHandler.removeCallbacks(mThread);
		}

		private void drawWallpaper(Canvas c) {
			float screenWidth = c.getWidth();
			float screenHeight = c.getHeight();
			Paint paint = new Paint();
			Random r = new Random();
			int nowHour = GregorianCalendar.getInstance().getTime().getHours() + 1;

			// fill sky
			if (nowHour < 6 || nowHour > 21)
				c.drawColor(COLOR_SKY_NIGHT);
			else
				c.drawColor(COLOR_SKY_DAY);

			// sun/moon
			if (nowHour < 6 || nowHour > 21) {
				paint = new Paint();
				paint.setColor(COLOR_MOON);
				paint.setStyle(Style.FILL_AND_STROKE);
				c.drawCircle(screenWidth * SCALE_SUN_X, screenHeight
						* SCALE_SUN_Y, screenWidth * SCALE_SUN_RADIUS, paint);
			} else {
				paint = new Paint();
				paint.setColor(COLOR_SUN_HALO);
				paint.setStyle(Style.FILL_AND_STROKE);
				paint.setAntiAlias(true);
				c.drawCircle(screenWidth * SCALE_SUN_X, screenHeight
						* SCALE_SUN_Y, screenWidth * SCALE_SUN_RADIUS * 1.2f,
						paint);
				paint = new Paint();
				paint.setColor(COLOR_SUN);
				paint.setStyle(Style.FILL_AND_STROKE);
				c.drawCircle(screenWidth * SCALE_SUN_X, screenHeight
						* SCALE_SUN_Y, screenWidth * SCALE_SUN_RADIUS, paint);
			}

			// ground
			paint = new Paint();
			paint.setColor(COLOR_GROUND);
			c.drawRect(0f, screenHeight * (1f - SCALE_GROUND_HEIGHT),
					screenWidth, screenHeight, paint);

			// grass
			paint = new Paint();
			paint.setColor(COLOR_GRASS);
			paint.setAntiAlias(true);
			paint.setStrokeWidth(1);
			paint.setStrokeCap(Paint.Cap.ROUND);
			paint.setStyle(Paint.Style.STROKE);
			for (int i = 0; i < screenWidth; i++) {
				float height = 0.05f * r.nextFloat();
				c.drawLine((float) i, screenHeight * SCALE_SKY_HEIGHT,
						(float) i, screenHeight * (SCALE_SKY_HEIGHT - height),
						paint);
			}

			// building
			paint = new Paint();
			paint.setColor(COLOR_BUILDING);
			float buildingLeft = screenWidth * SCALE_BUILDING_LEFT;
			float buildingRight = screenWidth * SCALE_BUILDING_RIGHT;
			float buildingTop = screenHeight
					* (1f - SCALE_GROUND_HEIGHT - SCALE_BUILDING_HEIGHT);
			float buildingBottom = screenHeight * (1f - SCALE_GROUND_HEIGHT);
			c.drawRect(buildingLeft, buildingTop, buildingRight,
					buildingBottom, paint);

			// windows on building
			paint = new Paint();
			float buildingWidth = screenWidth
					* (SCALE_BUILDING_RIGHT - SCALE_BUILDING_LEFT);
			float buildingHeight = screenWidth * SCALE_BUILDING_HEIGHT;
			float chop = 11;
			for (int bx = 1; bx < chop - 1; bx++)
				for (int by = 1; by < chop - 1; by++)
					if (bx % 2 == 1 && by % 2 == 1) {
						if (r.nextBoolean())
							paint.setColor(COLOR_WINDOW_DARK);
						else
							paint.setColor(COLOR_WINDOW_LIT);
						c.drawRect(buildingLeft + (buildingWidth / chop * bx),
								buildingTop + (buildingHeight / chop * by),
								buildingLeft
										+ (buildingWidth / chop * (bx + 1)),
								buildingTop
										+ (buildingHeight / chop * (by + 1)),
								paint);
					}

		}
	}
}
